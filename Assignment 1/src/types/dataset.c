#include "dataset.h"
#include "stdlib.h"

void freeDataSet(DataSet st) {
	DataPoint *d, *t;

	d = st.Head;
	while (d != NULL) {
		t = d->Next;
		free(d);
		d = t;
	}
	st.Head = NULL;
	st.Tail = NULL;
}

void addDataPoint(DataSet *st, DataPoint *dp) {
	st->Size += 1;
	if (st->Head == NULL) {
		st->Head = dp;
		st->Tail = dp;
		return;
	}
	st->Tail->Next = dp;
	st->Tail = st->Tail->Next;
}