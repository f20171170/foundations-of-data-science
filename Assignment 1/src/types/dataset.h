#ifndef TYPES_DATASET_H
#define TYPES_DATASET_H

#include "datapoint.h"

typedef struct DataSetStruct {
	size_t Size;
	DataPoint *Head;
	DataPoint *Tail;
} DataSet;

void freeDataSet(DataSet st);
void addDataPoint(DataSet *st, DataPoint *dp);

#endif