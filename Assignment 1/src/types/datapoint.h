#ifndef TYPES_DATAPOINT_H
#define TYPES_DATAPOINT_H

#include <stdio.h>
#include <stdbool.h>

/*
 * Data points from the Sydney weather dataset.
 *   INT_MAX - unavailable int
 *   FLT_MAX - unavailable float
 *   "" - unavailable string
*/
typedef struct DataPointStruct {
	char Date[BUFSIZ];
	char Location[BUFSIZ];
	float MinTemp;
	float MaxTemp;
	float Rainfall;
	float Evaporation;
	float Sunshine;
	char WindGustDir[BUFSIZ];
	char WindGustSpeed[BUFSIZ];
	char WindDir9am[BUFSIZ];
	char WindDir3pm[BUFSIZ];
	int WindSpeed9am;
	int WindSpeed3pm;
	int Humidity9am;
	int Humidity3pm;
	float Pressure9am;
	float Pressure3pm;
	int Cloud9am;
	int Cloud3pm;
	float Temp9am;
	float Temp3pm;
	bool RainToday;
	bool RainTomorrow;
	struct DataPointStruct *Next;
} DataPoint;

#endif