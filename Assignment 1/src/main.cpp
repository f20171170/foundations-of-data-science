#include <iostream>
using namespace std;

#include "util/readCsv.h"
extern "C" {
#include "util/conversions.h"
#include "basis/sigmoidal.h"
#include "basis/polynomial.h"
}

void sigmoidal() {
	DataSet st;
	string filename = "../datasets/sydney-weather-dataset.csv";
	readCSV(&st, filename);
	truncate_to_2015(&st);
	truncate_To_half(&st);

	sigfit(&st);

	freeDataSet(st);
}

void polynomial() {
	DataSet st;
	string filename = "../datasets/sydney-weather-dataset.csv";
	readCSV(&st, filename);
	truncate_to_2015(&st);

	polyfit(&st, 2);

	freeDataSet(st);
}

int main() {
	sigmoidal();
	return 0;
}