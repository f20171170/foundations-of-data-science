#include <math.h>
#include <stdlib.h>

#include "sigmoidal.h"
#include "../util/conversions.h"

#define EPOCHS 100000
#define LEARNING_RATE 0.0001

/*
 * Implementation of the standard sigmoidal function (the logistic function)
 * but accepting the following parameters:
 *     alpha: float = 0.0
 *     beta: float = 1.0
 *     gamma: float = 1.0
 *     delta: float = 0.0
 *
 *     positive alpha -> move to the right
 *     negative alpha -> move to the left
 *
 *     decrease beta (within 0 and 1) -> stretch out the s-curve horizontally
 *                                       (more gradual increase)
 *     increase beta (beyond 1) -> make the increasing part of the s-curve more
 *                                 like a line: |
 *
 *     decrease gamma (within 0 and 1) -> squish the function vertically
 *     increase gamma (beyond 1) -> stretch out the function vertically
 *
 *     positive delta -> move the function up
 *     negative delta -> move the function down
*/
double sigmoidal(double x, double alpha, double beta, double gamma,
		 double delta) {
	x = (x - alpha) * beta;
	double y = 1.0 / (1.0 + exp(-x));
	y = (y * gamma) + delta;
	return y;
}

double sig_mean_squared_error(DataSet *ds, double alpha, double beta, double gamma,
			  double delta) {
	double err = 0.0;
	for (DataPoint *p = ds->Head; p != NULL; p = p->Next) {
		double x = (double)get_day_from_date(p->Date);
		double y_obs = (double)p->Temp9am;
		double y_exp = sigmoidal(x, alpha, beta, gamma, delta);
		err += pow(y_exp - y_obs, 2);
	}
	err /= ds->Size;
	return err;
}

void determine_gradients(DataSet *ds, double alpha, double beta, double gamma,
			 double delta, double *gradients) {
	double a = 0.0, b = 0.0, c = 0.0, d = 0.0;
	for (DataPoint *p = ds->Head; p != NULL; p = p->Next) {
		double x = (double)get_day_from_date(p->Date);
		double y = (double)p->Temp9am;
		double t = exp(beta * (alpha - x));
		double z = y - (gamma / (1 + t)) - delta;
		a += z * (t / (pow(1 + t, 2)));
		b += z * (t * (alpha - x) / (pow(1 + t, 2)));
		c += z * (1 / (1 + t));
		d += z;
	}
	double tn = 2.0 / ds->Size;
	a *= (tn)*gamma * beta;
	b *= (tn)*gamma;
	c *= -tn;
	d *= -tn;
	gradients[0] = a;
	gradients[1] = b;
	gradients[2] = c;
	gradients[3] = d;
}

void sigfit(DataSet *ds) {
	double gradients[4];
	double alpha = 275.0, beta = 0.5, gamma = 20.0, delta = 7.0;
	double mse = sig_mean_squared_error(ds, alpha, beta, gamma, delta);
	printf("MSE before training: %lf\n", mse);
	printf("Parameters before training: <%lf, %lf, %lf, %lf>\n", alpha,
	       beta, gamma, delta);
	for (int e = 0; e < EPOCHS; e++) {
		determine_gradients(ds, alpha, beta, gamma, delta, gradients);
		alpha -= gradients[0] * LEARNING_RATE;
		beta -= gradients[1] * LEARNING_RATE;
		gamma -= gradients[2] * LEARNING_RATE;
		delta -= gradients[3] * LEARNING_RATE;
	}
	mse = sig_mean_squared_error(ds, alpha, beta, gamma, delta);
	printf("MSE after training: %lf\n", mse);
	printf("Parameters after training: <%lf, %lf, %lf, %lf>\n", alpha, beta,
	       gamma, delta);
}