#ifndef BASIS_POLYNOMIAL_H
#define BASIS_POLYNOMIAL_H

#include "../types/dataset.h"

float polynomial(int deg, int x, float *coefs);
void polyfit(DataSet *st, int deg);

#endif