#include <iostream>
#include <string>
#include <fstream>
#include <cmath>
#define _USE_MATH_DEFINES
#include <math.h>
#include <random>
#include "../types/dataset.h"
#include "../util/readCsv.cpp"
#define nofterms 3
#define alpha 0.0001
float fourier(int x, vector<float> &weights, int size) {
	float val = weights[0];
	float x_c = (float)x;
	x_c *= M_PI / 175;
	for (int i = 0; i < (nofterms - 1) / 2; i++) {
		val += weights[2 * i + 1] * sin((i + 1) * x_c);
		val += weights[2 * i + 2] * cos((i + 1) * x_c);
	}
	return val;
}
float get_random() {
	static default_random_engine e;
	static uniform_real_distribution<> dis(0, 1); // rage 0 - 1
	return dis(e);
}
double calculateError(vector<float> &prediction, vector<float> &mintemp) {
	double error = 0.0;
	for (int i = 0; i < mintemp.size(); i++) {
		error += pow((mintemp[i] - prediction[i]), 2);
		// cout<<"error is "<<error<<endl;
	}
	return error / mintemp.size();
}
void graidentDescent(vector<float> &prediction, vector<int> &dates,
		     vector<float> &mintemp, vector<float> &weights) {
	float change;
	float x;
	int c;
	for (int i = 0; i < nofterms; i++) {
		float dw = 0;
		for (int j = 0; j < mintemp.size(); j++) {
			change = mintemp[j] - prediction[j];
			c = float(int(i / 2) + 1);
			c *= M_PI / 175;
			if (i != 0) {
				x = (float)dates[j];
				if (i % 2 == 0) {
					change *= cos(c * x);
				} else {
					change *= sin(c * x);
				}
			}
			dw += change;
		}
		dw /= mintemp.size();
		// cout<<"dw is "<<dw;
		weights[i] -= 2 * alpha * dw;
	}
	return;
}
void fourierBasis() {
	string data("../datasets/sydney-weather-dataset.csv");
	DataSet ds;
	readCSV(&ds, data);
	vector<float> mintemp;
	vector<int> dates;
	DataPoint *dp;
	dp = ds.Head;
	float minVal = FLT_MAX;
	float maxVal = FLT_MIN;
	int date = 1;
	while (dp) {
		if (dp->MinTemp == FLT_MAX) {
			dp = dp->Next;
			date++;
			continue;
		}
		minVal = min(minVal, dp->MinTemp);
		mintemp.push_back(dp->MinTemp);
		dates.push_back(date);
		date++;
		maxVal = max(maxVal, dp->MinTemp);
		dp = dp->Next;
	}
	for (int i = 0; i < mintemp.size(); i++) {
		mintemp[i] -= minVal;
		mintemp[i] /= (maxVal - minVal);
		// cout<<mintemp[i]<<endl;
	}
	vector<float> weights;
	auto a = (unsigned)1617709282;
	srand(a);
	for (int i = 0; i < nofterms; i++) {
		float f = (float)rand() / RAND_MAX;
		weights.push_back(f);
	}
	vector<float> prediction(mintemp.size(), 0);
	for (int i = 0; i < dates.size(); i++) {
		prediction[i] = fourier(i, weights, mintemp.size());
	}
	//error with randomly initialized weights
	double error = calculateError(prediction, weights);
	cout << " error is " << error << endl;
	//gradient descent
	for (int i = 0; i < 1200; i++) {
		graidentDescent(prediction, dates, mintemp, weights);
		for (int i = 0; i < dates.size(); i++) {
			prediction[i] = fourier(i, weights, mintemp.size());
		}
		error = calculateError(prediction, weights);
		if (i % 50 == 0) {
			cout << "after " << i + 1 << " gradient descent step "
			     << " error is " << error << endl;
		}
	}
	cout << "Final Weights are :-\n";
	for (int i = 0; i < weights.size(); i++) {
		cout << weights[i];
		if (i != weights.size() - 1) {
			cout << ",";
		}
	}
	cout << endl;
	// cout<<"inital error is "<<init_error<<"  final error is "<<error<<endl;
}
int main(int argc, char **argv) {
	fourierBasis();
	return 0;
}