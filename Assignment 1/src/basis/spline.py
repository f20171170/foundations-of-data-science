import numpy as np  # ONLY for arrays
import matplotlib.pyplot as plt  # ONLY for data visualization

EPOCHS = 10000
LEARNING_RATE = 1e-4

plt.style.use("seaborn")
plt.axhline(color="black", lw=0.5)
plt.axvline(color="black", lw=0.5)

# Datapoints (observed points from 2015).
# fmt: off
observed = [
    25.2, 23.2, 26.3, 26.9, 21.8, 24.4, 27.1, 25.8, 25.7, 24.3, 21.1, 22.9, 21.2, 28.6, 26.1, 24.7, 27, 24.5, 21.7, 20.3, 24.4, 26.4, 25.8, 26.7, 25, 20.4, 19.2, 17.9, 19, 19.6, 21.4, 22, 19.6, 20.1, 20.1, 21.9, 20.5, 20.3, 23.3, 23.7, 24, 25.8, 26.1, 22.1, 23.1, 22.4, 25.4, 25.7, 26.4, 23, 23.8, 22.9, 23.8, 24.1, 23.3, 22.2, 21.5, 21.1, 23.3, 26.2, 20.1, 22.5, 23.7, 25.2, 21.1, 19.8, 21, 23.7, 22.8,
    22.5, 25, 20.8, 20.1, 21.8, 17.9, 20.4, 22.5, 22.8, 23.8, 19.6, 22.2, 22.8, 22.2, 18, 20.4, 17.2, 18.5, 17.3, 19.1, 19.7, 18.8, 20.3, 18.1, 18, 19.9, 19.9, 20, 18.3, 18.6, 16.8, 17.5, 18.7, 19.1, 18.2, 18.6, 21.6, 21, 23.7, 19.4, 15, 14.9, 16.1, 16.4, 17.3, 17.2, 13.8, 15.3, 14, 14, 16, 16.7, 21.7, 19, 18.3, 19, 15.3, 15.1, 13.8, 14.5, 15.4, 16.8, 16.2, 13.9, 11.3, 14.2, 15.1, 13, 13.8, 16.9, 17.4,
    16.5, 15.2, 11.8, 10.1, 11.4, 10.4, 13.1, 13.6, 17.3, 13.1, 12.1, 13.4, 9.5, 8.8, 8.9, 10.2, 11.9, 10.8, 14.8, 17.1, 12.9, 13.3, 13.5, 13.8, 11.2, 12.9, 16.5, 16, 13.5, 9.9, 9.8, 9.1, 9.3, 10.2, 13.2,
    15.2, 11.7, 9.7, 9.3, 11, 10.7, 12, 8.4, 8.1, 7.6, 7.7, 8.6, 11.2, 8.4, 8.3, 9.4, 7.2, 9.8, 8.6, 11.5, 8.7, 10.3, 6.7, 10.4, 10.3, 10, 9.9, 13.4, 12, 12.9, 14.3, 13.8, 10.2, 9.6, 9.3, 10.7, 14.7, 14.3, 16.9, 11.7, 8.9, 9.4, 9.5, 10, 11, 9.1, 11.8, 13.3, 11.1, 12.3, 11.8, 12.4, 14, 13.4, 12.6, 12.5, 12.1, 13, 19.8, 15.6, 16.3, 16.1, 16.3, 15.1, 14.6, 13, 12.1, 13.9, 12.7, 11.9, 14.4, 13.7, 15.2, 14.6, 17.4, 13.3, 15.4, 15.5, 17.6, 17.2, 18.3, 20.7, 23.5, 17.2, 18, 15.8, 14.7, 16.6, 16.3, 18.7, 13.3, 13, 13.4, 14.4, 15.3, 17.3, 19.1, 21, 19.9, 21.3, 21.5, 26.4, 21.1, 23.8, 20.3, 18.5, 20.1, 21.2, 18.7, 22.7, 18.3, 20.1, 21.6, 23.3, 21.5, 19.5, 21.8, 23.7, 24.5, 19.6, 16.6, 17.8, 21.1, 24.6, 16.5, 16.4, 18.2, 19, 22.2, 21.9, 21.6, 19, 19.5, 18.2, 23.3, 22.5, 18, 19.5, 20.6, 20, 23.5, 22.7, 18.7,
    18.2, 19.1, 20.2, 25.1, 23.1, 25.2, 20.8, 19.8, 22.4, 23.6, 22.4, 32.4, 21.1, 21.5, 21.1, 21.1, 26.3, 21, 19.4, 20.3, 21, 23, 23.2, 22.7, 24.6, 21.3, 24.4, 20.8, 21.4, 23.4, 23.1, 22.9, 21.6, 24.5, 26.3, 24.6, 27.1, 17.8, 21.8, 21.6, 21.8, 22.7, 17.1, 16.7, 21.2, 20.2, 21.3,
]
# fmt: on


def line(x, m, b):
    return m * x + b


def linear_fit(xvals, yvals):
    m, b = 0, 7
    for _ in range(EPOCHS):
        g1, g2 = 0, 0
        for x, y in zip(xvals, yvals):
            d = y - line(x, m, b)
            m -= -2 * x * d * LEARNING_RATE / len(xvals)
            b -= -2 * d * LEARNING_RATE / len(xvals)
    return m, b


def mean_center(xvals):
    avg = sum(xvals) / len(xvals)
    return xvals - avg


def mean_squared_error(xvals, yvals, m, b):
    err = 0.0
    for x, ob in zip(xvals, yvals):
        y = line(x, m, b)
        err += (ob - y) ** 2
    err /= len(xvals)
    return err


def spline_fit(xvals, yvals):
    xvals = mean_center(xvals)
    half_length = len(xvals) // 2
    m1, b1 = linear_fit(xvals[: half_length + 1], yvals[: half_length + 1])
    m2, b2 = linear_fit(xvals[half_length:], yvals[half_length:])
    b = (b1 + b2) / 2
    plt.plot(
        xvals[: half_length + 1],
        [line(x, m1, b) for x in xvals[: half_length + 1]],
        color="navy",
        label=f"{m1:.3f}x + b",
    )
    plt.plot(
        xvals[half_length:],
        [line(x, m2, b) for x in xvals[half_length:]],
        color="brown",
        label=f"{m2:.3f}x + b",
    )
    print(
        "Initial mean squared error: ",
        mean_squared_error(xvals[: half_length + 1], observed[: half_length + 1], 0, 7)
        + mean_squared_error(xvals[half_length:], observed[half_length:], 0, 7),
    )
    print(
        "Final mean squared error: ",
        mean_squared_error(xvals[: half_length + 1], yvals[: half_length + 1], m1, b)
        + mean_squared_error(xvals[half_length:], yvals[half_length:], m2, b),
    )


if __name__ == "__main__":
    xvals = np.linspace(1, 365, 365)
    plt.plot(
        mean_center(xvals),
        observed,
        marker=".",
        linestyle="",
        color="black",
        label="observed data",
    )
    spline_fit(xvals, observed)
    plt.legend()
    plt.show()
