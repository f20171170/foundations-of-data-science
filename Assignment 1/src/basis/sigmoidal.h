#ifndef BASIS_SIGMOIDAL_H
#define BASIS_SIGMOIDAL_H

#include "../types/dataset.h"

void sigfit(DataSet *ds);
double sigmoidal(double x, double alpha, double beta, double gamma,
		 double delta);

#endif