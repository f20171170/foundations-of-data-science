#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

#include "polynomial.h"
#include "../util/conversions.h"

#define EPOCHES 1000
#define LEARNING_RATE 1e-9

#define SEED_LB 1
#define SEED_UB 10

#define VERBOSE

float _square(float x) {
	return x * x;
}

/*
 * Seed random float values for n=deg parameters.
 * The returned pointer must be freed by the caller.
*/
float *_get_rand_coefs(int deg) {
	srand((unsigned int)time(0));
	float *coefs = (float *)malloc(sizeof(float) * (deg + 1));
#ifdef VERBOSE
	printf("Using seed values: ");
#endif
	for (int i = 0; i <= deg; i++) {
		coefs[i] = (float)((rand() + SEED_LB) % SEED_UB) + SEED_LB;
#ifdef VERBOSE
		printf("%.1f  ", coefs[i]);
#endif
	}
#ifdef VERBOSE
	printf("\n");
#endif
	return coefs;
}

float polynomial(int deg, int x, float *coefs) {
	if (deg == 0)
		return 0.0;
	int xp = 1;
	float val = coefs[0];
	for (int i = 1; i <= deg; i++) {
		xp *= x;
		val += coefs[i] * xp;
	}
	return val;
}

float poly_mean_squared_error(DataSet *st, float *xvals, int deg, float *coefs) {
	float err = 0.0;
	int i = 0;
	for (DataPoint *p = st->Head; p != NULL; p = p->Next) {
		int x = xvals[i];
		err += _square(p->Temp9am - polynomial(deg, x, coefs));
		i++;
	}
	err /= st->Size;
	return err;
}

float recommended_coefs[] = { 33.0, -0.18, 0.0004 };

void polyfit(DataSet *st, int deg) {
	int avg = 0;
	DataPoint *p;

	float *coefs = _get_rand_coefs(deg);

	p = st->Head;
	float *xvals = (float *)malloc(sizeof(float) * st->Size);
	for (int i = 0; i < st->Size; i++) {
		if (p == NULL) {
			printf("Size mismatch!\n");
		}
		xvals[i] = get_day_from_date(p->Date);
		avg += p->Temp9am;
		p = p->Next;
	}
	avg /= st->Size;
	for (int i = 0; i < st->Size; i++) {
		xvals[i] -= avg;
	}

	printf("MSE before training: %.3f\n",
	       poly_mean_squared_error(st, xvals, deg, coefs));
	printf("Parameters before training: ");
	for (int i = 0; i <= deg; i++) {
		printf("%f  ", coefs[i]);
	}
	printf("\n");

	for (int i = 0; i <= min(deg, 2); i++) {
		coefs[i] = recommended_coefs[i];
	}

	float *grads = (float *)malloc(sizeof(float) * (deg + 1));
	for (int e = 0; e < EPOCHES; e++) {
		for (int i = 0; i <= deg; i++) {
			grads[i] = 0;
		}
		for (DataPoint *p = st->Head; p != NULL; p = p->Next) {
			float y = p->Temp9am;
			int x = get_day_from_date(p->Date);
			float d = y - polynomial(deg, x, coefs);
			float xp = 1;
			grads[0] = d;
			for (int i = 1; i <= deg; i++) {
				xp *= x;
				grads[i] = coefs[i] * xp;
			}
		}
		int mltp = (-2.0) / st->Size;
		for (int i = 0; i <= deg; i++) {
			grads[i] *= mltp;
		}

		for (int i = 0; i <= deg; i++) {
			coefs[i] -= LEARNING_RATE * grads[i];
		}
	}

	printf("MSE after training: %.3f\n",
	       poly_mean_squared_error(st, xvals, deg, coefs));
	printf("Parameters after training: ");
	for (int i = 0; i <= deg; i++) {
		printf("%.10f  ", coefs[i]);
	}
	printf("\n");

	free(xvals);
	free(grads);
	free(coefs);
}