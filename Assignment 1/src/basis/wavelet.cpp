#include <iostream>
#include <string>
#include <fstream>
#include <cmath>
#include <random>
#include "../types/dataset.h"
#include "../util/readCsv.cpp"
#define nofwavelets 10
#define alpha 0.2
float sigma = 70;
float wavelet(int x, int mu) {
	float val = pow((x - mu), 2) / (2 * pow(sigma, 2));
	return (float)exp(-1 * val);
}
float get_random() {
	static default_random_engine e;
	static uniform_real_distribution<> dis(0, 1); // rage 0 - 1
	return dis(e);
}
double calculateError(vector<float> &final_prediction,
		      vector<vector<float> > &predicted, vector<float> &mintemp,
		      vector<float> &weights) {
	double error = 0.0;
	float prediction;
	for (int i = 0; i < mintemp.size(); i++) {
		prediction = 0.0;
		for (int j = 0; j < nofwavelets; j++) {
			prediction += weights[j] * predicted[j][i];
		}
		final_prediction[i] = prediction;
		error += pow((mintemp[i] - prediction), 2);
		// cout<<"error is "<<error<<endl;
	}
	return error / mintemp.size();
}
void graidentDescent(vector<float> &final_prediction, vector<float> &mintemp,
		     vector<int> &dates, vector<int> &means,
		     vector<float> &weights) {
	float change;
	for (int i = 0; i < nofwavelets; i++) {
		float dw = 0;
		for (int j = 0; j < mintemp.size(); j++) {
			change = final_prediction[j] - mintemp[j];
			change *= wavelet(dates[j], means[i]);
			dw += change;
		}
		dw /= mintemp.size();
		weights[i] -= alpha * dw;
	}
	return;
}
void waveletBasis() {
	string data("../datasets/sydney-weather-dataset.csv");
	DataSet ds;
	readCSV(&ds, data);
	vector<float> mintemp;
	vector<int> dates;
	DataPoint *dp;
	dp = ds.Head;
	float minVal = FLT_MAX;
	float maxVal = FLT_MIN;
	int date = 1;
	while (dp) {
		if (dp->MinTemp == FLT_MAX) {
			dp = dp->Next;
			date++;
			continue;
		}
		minVal = min(minVal, dp->MinTemp);
		mintemp.push_back(dp->MinTemp);
		dates.push_back(date);
		date++;
		maxVal = max(maxVal, dp->MinTemp);
		dp = dp->Next;
	}
	for (int i = 0; i < mintemp.size(); i++) {
		mintemp[i] -= minVal;
		mintemp[i] /= (maxVal - minVal);
		// cout<<mintemp[i]<<endl;
	}
	int slope = (dates[dates.size() - 1] - dates[0]) / (nofwavelets);
	vector<int> means;
	for (int i = 0; i < nofwavelets; i++) {
		means.push_back(slope * i + dates[0]);
	}
	vector<vector<float> > predicted(nofwavelets,
					 vector<float>(mintemp.size(), 0));
	for (int i = 0; i < nofwavelets; i++) {
		for (int j = 0; j < mintemp.size(); j++) {
			predicted[i][j] = wavelet(dates[j], means[i]);
		}
	}
	vector<float> weights;
	for (int i = 0; i < nofwavelets; i++) {
		weights.push_back(1);
	}
	vector<float> final_prediction(mintemp.size(), 0);
	//error with randomly initialized weights
	double error =
		calculateError(final_prediction, predicted, mintemp, weights);
	cout << "sigma is " << sigma << " error is " << error << endl;
	//gradient descent
	for (int i = 0; i < 500; i++) {
		graidentDescent(final_prediction, mintemp, dates, means,
				weights);
		error = calculateError(final_prediction, predicted, mintemp,
				       weights);
		if (i % 20 == 0) {
			cout << "after " << i + 1 << " gradient descent step "
			     << " error is " << error << endl;
		}
	}
	cout << "Final Weights are :" << endl;
	for (int i = 0; i < weights.size(); i++) {
		cout << weights[i];
		if (i != weights.size() - 1) {
			cout << ",";
		}
	}
	cout << endl;
	cout << "Means are: " << endl;
	for (int i = 0; i < means.size(); i++) {
		cout << means[i];
		if (i != means.size() - 1) {
			cout << ",";
		}
	}
	cout << endl;
}
int main(int argc, char **argv) {
	waveletBasis();
	return 0;
}