import math
from typing import Dict

import matplotlib.pyplot as plt
import numpy as np

EPOCHS = 100000
LEARNING_RATE = 1e-4

plt.style.use("seaborn")
plt.axhline(color="black", lw=0.5)
plt.axvline(color="black", lw=0.5)

# Datapoints (observed points from 2015)
# fmt: off
observed = [
    25.2, 23.2, 26.3, 26.9, 21.8, 24.4, 27.1, 25.8, 25.7, 24.3, 21.1, 22.9, 21.2, 28.6, 26.1, 24.7, 27, 24.5, 21.7, 20.3, 24.4, 26.4, 25.8, 26.7, 25, 20.4, 19.2, 17.9, 19, 19.6, 21.4, 22, 19.6, 20.1, 20.1, 21.9, 20.5, 20.3, 23.3, 23.7, 24, 25.8, 26.1, 22.1, 23.1, 22.4, 25.4, 25.7, 26.4, 23, 23.8, 22.9, 23.8, 24.1, 23.3, 22.2, 21.5, 21.1, 23.3, 26.2, 20.1, 22.5, 23.7, 25.2, 21.1, 19.8, 21, 23.7, 22.8,
    22.5, 25, 20.8, 20.1, 21.8, 17.9, 20.4, 22.5, 22.8, 23.8, 19.6, 22.2, 22.8, 22.2, 18, 20.4, 17.2, 18.5, 17.3, 19.1, 19.7, 18.8, 20.3, 18.1, 18, 19.9, 19.9, 20, 18.3, 18.6, 16.8, 17.5, 18.7, 19.1, 18.2, 18.6, 21.6, 21, 23.7, 19.4, 15, 14.9, 16.1, 16.4, 17.3, 17.2, 13.8, 15.3, 14, 14, 16, 16.7, 21.7, 19, 18.3, 19, 15.3, 15.1, 13.8, 14.5, 15.4, 16.8, 16.2, 13.9, 11.3, 14.2, 15.1, 13, 13.8, 16.9, 17.4,
    16.5, 15.2, 11.8, 10.1, 11.4, 10.4, 13.1, 13.6, 17.3, 13.1, 12.1, 13.4, 9.5, 8.8, 8.9, 10.2, 11.9, 10.8, 14.8, 17.1, 12.9, 13.3, 13.5, 13.8, 11.2, 12.9, 16.5, 16, 13.5, 9.9, 9.8, 9.1, 9.3, 10.2, 13.2,
    15.2, 11.7, 9.7, 9.3, 11, 10.7, 12, 8.4, 8.1, 7.6, 7.7, 8.6, 11.2, 8.4, 8.3, 9.4, 7.2, 9.8, 8.6, 11.5, 8.7, 10.3, 6.7, 10.4, 10.3, 10, 9.9, 13.4, 12, 12.9, 14.3, 13.8, 10.2, 9.6, 9.3, 10.7, 14.7, 14.3, 16.9, 11.7, 8.9, 9.4, 9.5, 10, 11, 9.1, 11.8, 13.3, 11.1, 12.3, 11.8, 12.4, 14, 13.4, 12.6, 12.5, 12.1, 13, 19.8, 15.6, 16.3, 16.1, 16.3, 15.1, 14.6, 13, 12.1, 13.9, 12.7, 11.9, 14.4, 13.7, 15.2, 14.6, 17.4, 13.3, 15.4, 15.5, 17.6, 17.2, 18.3, 20.7, 23.5, 17.2, 18, 15.8, 14.7, 16.6, 16.3, 18.7, 13.3, 13, 13.4, 14.4, 15.3, 17.3, 19.1, 21, 19.9, 21.3, 21.5, 26.4, 21.1, 23.8, 20.3, 18.5, 20.1, 21.2, 18.7, 22.7, 18.3, 20.1, 21.6, 23.3, 21.5, 19.5, 21.8, 23.7, 24.5, 19.6, 16.6, 17.8, 21.1, 24.6, 16.5, 16.4, 18.2, 19, 22.2, 21.9, 21.6, 19, 19.5, 18.2, 23.3, 22.5, 18, 19.5, 20.6, 20, 23.5, 22.7, 18.7,
    18.2, 19.1, 20.2, 25.1, 23.1, 25.2, 20.8, 19.8, 22.4, 23.6, 22.4, 32.4, 21.1, 21.5, 21.1, 21.1, 26.3, 21, 19.4, 20.3, 21, 23, 23.2, 22.7, 24.6, 21.3, 24.4, 20.8, 21.4, 23.4, 23.1, 22.9, 21.6, 24.5, 26.3, 24.6, 27.1, 17.8, 21.8, 21.6, 21.8, 22.7, 17.1, 16.7, 21.2, 20.2, 21.3,
]
# fmt: on

"""
Parameters:
    alpha: float = 0.0,  # The horizontal shift.
    beta: float = 0.0,  # The horizontal scaling factor.
    gamma: float = 0.0,  # The vertical scaling factor.
    delta: float = 0.0,  # The vertical shift.

    positive alpha -> move to the right
    negative alpha -> move to the left

    decrease beta (within 0 and 1) -> stretch out the s-curve horizontally (more gradual increase)
    increase beta (beyond 1) -> make the increasing part of the s-curve more like a line: |

    decrease gamma (within 0 and 1) -> squish the function vertically
    increase gamma (beyond 1) -> stretch out the function vertically

    positive delta -> move the function up
    negative delta -> move the function down
"""


def sigmoidal_function(x: float, parameters: Dict[str, float]) -> float:
    x = (x - parameters["alpha"]) * parameters["beta"]  # Adjust x (horizontal)
    y = 1.0 / (1.0 + math.e ** -x)  # Compute sigmoid
    y = (y * parameters["gamma"]) + parameters["delta"]  # Adjust the result (vertical)
    return y


def mean_squared_error(xvals: list[float], parameters: Dict[str, float]) -> float:
    err = 0.0
    for x in xvals:
        y = sigmoidal_function(x, parameters)
        err += (observed[x - 1] - y) ** 2
    err /= len(xvals)
    return err


def determine_gradients(
    xvals: list[float], yvals: list[float], parameters: Dict[str, float]
) -> Dict[str, float]:
    n = len(xvals)
    gradients = {"alpha": 0.0, "beta": 0.0, "gamma": 0.0, "delta": 0.0}
    for x, y in zip(xvals, yvals):
        t = math.e ** (parameters["beta"] * (parameters["alpha"] - x))
        z = y - (parameters["gamma"] / (1 + t)) - parameters["delta"]
        gradients["alpha"] += z * (t / ((1 + t) ** 2))
        gradients["beta"] += z * (t * (parameters["alpha"] - x) / ((1 + t) ** 2))
        gradients["gamma"] += z * (1 / (1 + t))
        gradients["delta"] += z
    tn = 2 / n
    gradients["alpha"] *= (tn) * parameters["gamma"] * parameters["beta"]
    gradients["beta"] *= (tn) * parameters["gamma"]
    gradients["gamma"] *= -tn
    gradients["delta"] *= -tn
    return gradients


def main():
    # Plot the observed points
    midpoint = 365 // 2
    xvals = list(range(1, 366, 1))[midpoint:]
    yvals = observed[midpoint:]
    plt.plot(
        xvals, yvals, marker=".", linestyle="", color="black", label="observed data"
    )

    # Plot the sigmoidal function centered at the second half of the data
    # (with our initial values for the parameters). We don't stretch it
    # out horizontally yet. That's what we're leaving for gradient descent
    # to determine
    parameters = {"alpha": 275.0, "beta": 1.0, "gamma": 20.0, "delta": 7.0}
    plt.plot(
        xvals,
        [sigmoidal_function(x, parameters) for x in xvals],
        color="purple",
        label="before",
    )
    prior_mse = mean_squared_error(xvals, parameters)
    print("MSE before training: ", prior_mse)

    # Apply gradient descent
    for _ in range(EPOCHS):
        gradients = determine_gradients(xvals, yvals, parameters)
        for k in parameters:
            parameters[k] -= LEARNING_RATE * gradients[k]

    # Plot the results after applying gradient descent
    plt.plot(
        xvals,
        [sigmoidal_function(x, parameters) for x in xvals],
        color="red",
        label="after (Python implementation)",
    )
    posterior_mse = mean_squared_error(xvals, parameters)
    print("MSE after training:  ", posterior_mse)
    print("Parameters: ", parameters)

    if posterior_mse < prior_mse:
        print("Gradient descent successful!!!")
    else:
        print("Gradient descent failed.")

    # Plot values obtained the C implementation
    parameters = {
        "alpha": 273.934838,
        "beta": 0.023041,
        "gamma": 15.876333,
        "delta": 9.329837,
    }
    plt.plot(
        xvals,
        [sigmoidal_function(x, parameters) for x in xvals],
        color="blue",
        label="after (C implementation)",
    )
    posterior_mse = mean_squared_error(xvals, parameters)
    print("MSE from C implementation:  ", posterior_mse)

    # Open the plot in a new window
    plt.legend()
    plt.show()


def show_basic_sigmoidal():
    xvals = np.linspace(-5, 5, 10000)
    plt.plot(
        xvals,
        [
            sigmoidal_function(
                x, {"alpha": 0.0, "beta": 1.0, "gamma": 1.0, "delta": 0.0}
            )
            for x in xvals
        ],
        color="black",
        label="sigmoid",
    )
    plt.show()


if __name__ == "__main__":
    main()