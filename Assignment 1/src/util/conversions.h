#ifndef UTIL_CONVERSIONS_H
#define UTIL_CONVERSIONS_H

#include "../types/dataset.h"

void truncate_to_2015(DataSet *ds);
void truncate_To_half(DataSet *ds);
int get_day_from_date(char *date);

#endif