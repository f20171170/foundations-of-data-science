#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <cstring>
#include <climits>
#include <cfloat>
using namespace std;

#include "readCsv.h"

bool to_bool(string str) {
	transform(str.begin(), str.end(), str.begin(), ::tolower);
	string yes = "yes";
	if (str == yes) {
		return true;
	}
	return false;
}

void readCSV(DataSet *ds, string file) {
	fstream fin(file.c_str());
	if (!fin.is_open()) {
		cout << "Unable to read the file" << endl;
		exit(0);
		return;
	}
	vector<int> intPos = { 11, 12, 13, 14, 17, 18 };
	vector<int> floatPos = { 2, 3, 4, 5, 6, 15, 16, 19, 20 };
	vector<int> stringPos = { 0, 1, 7, 8, 9, 10, 21, 22 };
	vector<string> row;
	string line, word, temp;
	DataPoint *dp;
	dp = new DataPoint();
	ds->Head = dp;
	int i = 0;
	fin >> temp;
	while (fin >> temp) {
		if (i != 0) {
			dp->Next = new DataPointStruct();
			dp = dp->Next;
		}
		i++;
		row.clear();
		// store it in a string variable 'line'
		line = temp;
		// used for breaking words
		stringstream s(line);

		// read every column data of a row and
		// store it in a string variable, 'word'

		while (getline(s, word, ',')) {
			// add all the column data
			// of a row to a vector
			row.push_back(word);
		}
		for (auto j : intPos) {
			if (row[j] == "NA") {
				row[j] = to_string(INT_MAX);
			}
		}
		for (auto j : floatPos) {
			if (row[j] == "NA") {
				row[j] = to_string(FLT_MAX);
			}
		}
		for (auto j : stringPos) {
			if (row[j] == "NA") {
				row[j] = "";
			}
		}
		strcpy(dp->Date, row[0].c_str());
		strcpy(dp->Location, row[1].c_str());
		dp->MinTemp = stof(row[2]);
		dp->MaxTemp = stof(row[3]);
		dp->Rainfall = stof(row[4]);
		dp->Evaporation = stof(row[5]);
		dp->Sunshine = stof(row[6]);
		strcpy(dp->WindGustDir, row[7].c_str());
		strcpy(dp->WindGustSpeed, row[8].c_str());
		strcpy(dp->WindDir9am, row[9].c_str());
		strcpy(dp->WindDir3pm, row[10].c_str());
		dp->WindSpeed9am = stoi(row[11]);
		dp->WindSpeed3pm = stoi(row[12]);
		dp->Humidity9am = stoi(row[13]);
		dp->Humidity3pm = stoi(row[14]);
		dp->Pressure9am = stof(row[15]);
		dp->Pressure3pm = stof(row[16]);
		dp->Cloud9am = stoi(row[17]);
		dp->Cloud3pm = stoi(row[18]);
		dp->Temp9am = stof(row[19]);
		dp->Temp3pm = stof(row[20]);
		dp->RainToday = to_bool(row[21]);
		dp->RainTomorrow = to_bool(row[22]);
	}
	ds->Tail = dp;
	ds->Size = i;
	return;
}