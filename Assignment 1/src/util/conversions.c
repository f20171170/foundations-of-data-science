#include <stdlib.h>

#include "conversions.h"

// Assumption: The date is in the right format.
bool _from_2015(char *date) {
	char want[] = "2015";
	for (int i = 0; i < 4; i++) {
		if (date[i] != want[i])
			return false;
	}
	return true;
}

void truncate_to_2015(DataSet *ds) {
	DataPoint *t = NULL;
	DataPoint *p = ds->Head;

	// Everything before.
	while (!_from_2015(p->Date) && p != NULL) {
		t = p->Next;
		free(p);
		p = t;
	}

	if (p == NULL) {
		ds->Head = NULL;
		ds->Tail = NULL;
		ds->Size = 0;
	}

	// Everything in range.
	ds->Size = 0;
	ds->Head = p;
	while (_from_2015(p->Date) && p != NULL) {
		ds->Size += 1;
		t = p;
		p = p->Next;
	}
	t->Next = NULL;
	ds->Tail = t;

	// Everything after.
	while (p != NULL) {
		t = p->Next;
		free(p);
		p = t;
	}
}

void truncate_To_half(DataSet *ds) {
	DataPoint *p = ds->Head, *t;
	size_t n = ds->Size / 2;
	ds->Size -= n;
	while (n-- && p != NULL) {
		t = p->Next;
		free(p);
		p = t;
	}
	ds->Head = p;
}

int _month_days[] = {
	31, // Jan
	28, // Feb
	31, // Mar
	30, // Apr
	31, // May
	30, // Jun
	31, // Jul
	31, // Aug
	30, // Sep
	31, // Oct
	30, // Nov
	31, // Dec
};

// Assumption: Date is of the right format. Also, not a leap year.
int get_day_from_date(char *date) {
	char buf[3];
	int month, day;

	// Skip "year-"
	date += 5;

	// Read the month.
	for (int i = 0; i < 2; i++) {
		buf[i] = date[i];
	}
	buf[2] = '\0';
	month = atoi(buf) - 1;

	// Skip "mo-"
	date += 3;

	// Read the day of the month.
	for (int i = 0; i < 2; i++) {
		buf[i] = date[i];
	}
	day = atoi(buf);

	// Compute offset from start of the year.
	for (int i = 0; i < month; i++) {
		day += _month_days[i];
	}

	return day;
}