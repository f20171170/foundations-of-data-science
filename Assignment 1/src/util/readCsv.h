#ifndef UTIL_READ_CSV_H
#define UTIL_READ_CSV_H

#include <string>
using namespace std;

extern "C" {
#include "../types/dataset.h"
}

bool to_bool(string str);
void readCSV(DataSet *ds, string file);

#endif