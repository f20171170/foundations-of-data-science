# Assignment 1

## About
- **Due Date:** April 3rd, 2021
- **Weightage:** 15%
- **Aim:**
Study and implement basis functions to understand their role in Data Science.

- **Tasks:**
You need to implement the following basis functions:
	1. Polynomial
	2. Sigmoidal
	3. Gaussian
	4. Fourier
	5. Splines (including B-splines)
	6. Wavelets

- **Submission Requirements:**
Submit in a single zip file. Name of the zip file should be concatenation
of the last three (four) digits of the idnos of the groups members.
	1. A report on basis functions containing theoretical aspects,
	implementation details, and results.
	2. Your code (in C/C++/Java).

## Group Members
1. Krishna Suravarapu (2017A7PS0237P)
2. Hemanth Alluri     (2017A7PS1170P)

## Building
### Prerequisites:
- One of:
	- GCC (g++)
	- Clang (clang++)
	- MSVC (cl)
- CMake (version 3.10.0 onward)
- Ninja (version 1.10.0 onward) (optional)

### Steps:
```bash
mkdir build
cd build
cmake -G Ninja ..
cmake --build .
```