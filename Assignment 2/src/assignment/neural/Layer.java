package assignment.neural;

import java.util.ArrayList;

import assignment.activation.ActivationFunction;

public class Layer {
	public Layer prevLayer;
	public Layer nextLayer;
	public ArrayList<Perceptron> nodes;

	public ActivationFunction afunc;
	public ArrayList<Float> activations; // store the last output value by each node in this layer.
	public float alpha; // learning rate.

	public Layer(int numNodes, ActivationFunction afunc, Layer prev, Layer next, float alpha) {
		this.afunc = afunc;
		this.prevLayer = prev;
		this.nextLayer = next;
		this.activations = new ArrayList<Float>();
		this.alpha = alpha;
		if (!this.isInputLayer()) {
			for (int i = 0; i < numNodes; i++) {
				activations.add(0.f);
			}
		}
		this.nodes = new ArrayList<Perceptron>(numNodes);
		for (int i = 0; i < numNodes; i++) {
			int prevNumNodes = 0;
			if (this.prevLayer != null)
				prevNumNodes = prevLayer.nodes.size();
			this.nodes.add(new Perceptron(prevNumNodes));
		}
	}

	public boolean isInputLayer() {
		return this.prevLayer == null;
	}

	public boolean isOutputLayer() {
		return this.nextLayer == null;
	}

	public ArrayList<Float> propagate(ArrayList<Float> input) {
		ArrayList<Float> output;
		if (this.isInputLayer()) {
			output = input;
			this.activations = input;
		} else {
			output = new ArrayList<Float>(nodes.size());
			for (int i = 0; i < nodes.size(); i++) {
				Perceptron node = this.nodes.get(i);
				float net = node.evaluate(input); // weighted sum + bias.
				float out = this.afunc.activate(net);
				this.activations.set(i, out);
				output.add(out);
			}
		}
		if (this.isOutputLayer()) {
			return output;
		} else {
			return nextLayer.propagate(output);
		}
	}

	public ArrayList<Float> backPropagate(ArrayList<Float> expected) {
		ArrayList<Float> errors = new ArrayList<Float>();

		for (int i = 0; i < this.nodes.size(); i++) {
			Perceptron node = this.nodes.get(i);
			float output = this.activations.get(i);
			float error;

			// The error term is computed differently for the output
			// layer than the hidden layers.
			if (this.isOutputLayer()) {
				error = (output - expected.get(i)) * this.afunc.derivative(output);
			} else {
				error = 0;
				for (int j = 0; j < this.nextLayer.nodes.size(); j++) {
					Perceptron nextLayerNode = this.nextLayer.nodes.get(j);
					error += expected.get(j) * nextLayerNode.weights.get(i) * this.afunc.derivative(output);
				}
			}
			errors.add(error);

			// Update weights leading into this layer and the biases of each node.
			for (int j = 0; j < this.prevLayer.nodes.size(); j++) {
				float layerInp = this.prevLayer.activations.get(j);
				float oldWeightVal = node.weights.get(j);
				float newWeightVal = oldWeightVal - this.alpha * (layerInp * error);
				node.weights.set(j, newWeightVal);
			}
			node.bias = node.bias - this.alpha * error;
		}

		if (this.prevLayer.isInputLayer()) {
			return errors;
		} else {
			return this.prevLayer.backPropagate(errors);
		}
	}
}
