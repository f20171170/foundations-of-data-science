package assignment.neural;

import java.util.ArrayList;

/* A single perceptron in the MLP model.
 * The actual connections to the previous layer and the following layer
 * will be handled by the layer object using this. All we need to store
 * here are the things unique to the perceptron (the bias, weights).
 * The layer object will also store the activation function since we don't
 * want to have different perceptrons in the same layer having different
 * activation functions.
*/
public class Perceptron {
	public float bias;
	public ArrayList<Float> weights;

	public Perceptron(int numWeights) {
		this.bias = (float) Math.random();
		this.weights = new ArrayList<Float>(numWeights);
		for (int i = 0; i < numWeights; i++) {
			this.weights.add((float) Math.random());
		}
	}

	// Applying the activation function is handled by the layer.
	public float evaluate(ArrayList<Float> inputs) {
		if (inputs.size() != this.weights.size()) {
			throw new IllegalArgumentException("Number of inputs and weights mismatch.");
		}

		float sum = this.bias;
		for (int i = 0; i < inputs.size(); i++) {
			sum += inputs.get(i) * this.weights.get(i);
		}
		return sum;
	}
}
