package assignment.neural;

import java.util.ArrayList;

import assignment.activation.ActivationFunction;

public class MLP {
	public Layer inputLayer;
	public Layer outputLayer;
	public final static float alpha = (float) 0.0001; // Learning rate.

	public MLP(ActivationFunction afunc, ActivationFunction afuncfinal, int numParams, int numHiddenLayers,
			int nodesPerHiddenLayer, int numClasses) {
		Layer inputLayer = new Layer(numParams, afunc, null, null, alpha);

		Layer cursor = inputLayer;
		for (int i = 0; i < numHiddenLayers; i++) {
			Layer newLayer = new Layer(nodesPerHiddenLayer, afunc, cursor, null, alpha);
			cursor.nextLayer = newLayer;
			cursor = newLayer;
		}
		cursor.nextLayer = new Layer(numClasses, afuncfinal, cursor, null, alpha);

		this.inputLayer = inputLayer;
		this.outputLayer = cursor.nextLayer;
	}

	public void train(ArrayList<ArrayList<Float>> data, ArrayList<ArrayList<Float>> labels, int numEpochs) {
		for (int k = 0; k < numEpochs; k++) {
			for (int i = 0; i < data.size(); i++) {
				ArrayList<Float> datapoint = data.get(i);
				ArrayList<Float> label = labels.get(i);
				inputLayer.propagate(datapoint);
				outputLayer.backPropagate(label);
			}
		}
	}

	public void test(ArrayList<ArrayList<Float>> data, ArrayList<ArrayList<Float>> labels) {
		int numCorrect = 0;
		for (int i = 0; i < data.size(); i++) {
			ArrayList<Float> datapoint = data.get(i);
			ArrayList<Float> label = labels.get(i);
			ArrayList<Float> outputs = inputLayer.propagate(datapoint);
			float maxOut = outputs.get(0);
			int maxOutIdx = 0;
			for (int j = 1; j < outputs.size(); j++) {
				if (outputs.get(j) > maxOut) {
					maxOut = outputs.get(j);
					maxOutIdx = j;
				}
			}

			float maxLabel = label.get(0);
			int maxLabelIdx = 0;
			for (int j = 1; j < label.size(); j++) {
				if (label.get(j) > maxLabel) {
					maxLabel = label.get(j);
					maxLabelIdx = j;
				}
			}

			if (maxOutIdx == maxLabelIdx) {
				numCorrect += 1;
			}
		}

		float accuracy = (float) numCorrect * 100 / data.size();
		System.out.printf("Number correct: %d / %d  (%.4f%% accuracy)\n", numCorrect, data.size(), accuracy);
	}
}
