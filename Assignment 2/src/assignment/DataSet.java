package assignment;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class DataSet {
	public ArrayList<DataPoint> completeSet;

	public ArrayList<ArrayList<Float>> trainingData;
	public ArrayList<Integer> trainingLabels;

	public ArrayList<ArrayList<Float>> testData;
	public ArrayList<Integer> testLabels;

	public DataSet() {
		this.completeSet = new ArrayList<DataPoint>();
		this.trainingData = new ArrayList<ArrayList<Float>>();
		this.trainingLabels = new ArrayList<Integer>();
		this.testData = new ArrayList<ArrayList<Float>>();
		this.testLabels = new ArrayList<Integer>();
	}

	public void loadIrisDataSet() throws NumberFormatException, IOException {
		String line;
		BufferedReader f = new BufferedReader(new FileReader("iris.csv"));
		f.readLine(); // Read the labels line and discard.
		try {
			while ((line = f.readLine()) != null) {
				DataPoint p = new DataPoint();
				String[] parts = line.split(",");
				p.sepalLength = Float.parseFloat(parts[0]);
				p.sepalWidth = Float.parseFloat(parts[1]);
				p.petalLength = Float.parseFloat(parts[2]);
				p.petalWidth = Float.parseFloat(parts[3]);
				p.species = parts[4].strip();
				this.completeSet.add(p);
			}
		} finally {
			f.close();
		}

		if (this.completeSet.size() != 150) {
			throw new IllegalArgumentException("Expected 150 datapoints.");
		}

		// Now partition the dataset into training and test data.
		for (int offset = 0; offset < 3; offset++) {
			for (int i = 0; i < 40; i++) {
				DataPoint p = this.completeSet.get(offset * 50 + i);
				Float[] arr = { p.sepalLength, p.sepalWidth, p.petalLength, p.petalWidth };
				ArrayList<Float> data = new ArrayList<>(Arrays.asList(arr));
				int label = p.getClassID();
				this.trainingData.add(data);
				this.trainingLabels.add(label);
			}
			for (int i = 40; i < 50; i++) {
				DataPoint p = this.completeSet.get(offset * 50 + i);
				Float[] arr = { p.sepalLength, p.sepalWidth, p.petalLength, p.petalWidth };
				ArrayList<Float> data = new ArrayList<>(Arrays.asList(arr));
				int label = p.getClassID();
				this.testData.add(data);
				this.testLabels.add(label);
			}
		}
	}
}
