package assignment;

public class DataPoint {
	public float sepalLength;
	public float sepalWidth;
	public float petalLength;
	public float petalWidth;
	public String species;

	@Override
	public String toString() {
		return String.format("%.1f,%.1f,%.1f,%.1f,%s", this.sepalLength, this.sepalWidth, this.petalLength,
				this.petalWidth, this.species);
	}

	public int getClassID() {
		if (this.species.equals("Iris-setosa")) {
			return 0;
		} else if (this.species.equals("Iris-versicolor")) {
			return 1;
		} else {
			return 2;
		}
	}
}
