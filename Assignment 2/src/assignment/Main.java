package assignment;

import java.io.IOException;
import java.util.ArrayList;

import assignment.neural.MLP;
import assignment.activation.Sigmoidal;
import assignment.activation.Relu;

public class Main {
	final static int numHiddenLayers = 2;
	final static int nodesPerHiddenLayer = 6;
	final static int numParams = 4;
	final static int numClasses = 3;
	final static int numEpochs = 10000;

	public static ArrayList<Float> labelFromClassID(int b) {
		ArrayList<Float> label = new ArrayList<>();
		if (b == 0) {
			label.add(1.f);
			label.add(0.f);
			label.add(0.f);
		} else if (b == 1) {
			label.add(0.f);
			label.add(1.f);
			label.add(0.f);
		} else {
			label.add(0.f);
			label.add(0.f);
			label.add(1.f);
		}
		return label;
	}

	public static void main(String[] args) throws NumberFormatException, IOException {
		DataSet dataset = new DataSet();
		dataset.loadIrisDataSet();
		MLP neuralNet = new MLP(new Relu(), new Sigmoidal(), numParams, numHiddenLayers, nodesPerHiddenLayer,
				numClasses);

		ArrayList<ArrayList<Float>> trainingLabels = new ArrayList<>();
		for (int i = 0; i < dataset.trainingLabels.size(); i++) {
			trainingLabels.add(labelFromClassID(dataset.trainingLabels.get(i)));
		}
		ArrayList<ArrayList<Float>> testLabels = new ArrayList<>();
		for (int i = 0; i < dataset.testLabels.size(); i++) {
			testLabels.add(labelFromClassID(dataset.testLabels.get(i)));
		}

		System.out.println("Against the training data before training:");
		neuralNet.test(dataset.trainingData, trainingLabels);
		System.out.println("\nAgainst the test data before training:");
		neuralNet.test(dataset.testData, testLabels);

		// Training.
		neuralNet.train(dataset.trainingData, trainingLabels, numEpochs);

		System.out.println("\nAgainst the training data after training (1 - training error):");
		neuralNet.test(dataset.trainingData, trainingLabels);
		System.out.println("\nAgainst the test data after training (1 - generalization error):");
		neuralNet.test(dataset.testData, testLabels);
	}
}
