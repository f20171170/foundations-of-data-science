package assignment.activation;

public class Relu implements ActivationFunction {

	@Override
	public float activate(float x) {
		if (x < 0) {
			return 0.f;
		} else {
			return x;
		}
	}

	@Override
	public float derivative(float x) {
		if (x < 0) {
			return 0.f;
		} else {
			return 1.f;
		}
	}

}
