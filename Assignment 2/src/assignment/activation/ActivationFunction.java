package assignment.activation;

/* Univariate, continuous and differentiable functions. */
public interface ActivationFunction {
	/* Evaluate the function at the given point. */
	public float activate(float x);

	/* Measure the derivative of the function at the given point. */
	public float derivative(float x);
}
