package assignment.activation;

public class Step implements ActivationFunction {
	public float threshold;

	public Step(float x) {
		this.threshold = x;
	}

	@Override
	public float activate(float x) {
		if (x < this.threshold) {
			return 0.f;
		} else {
			return 1.f;
		}
	}

	@Override
	public float derivative(float x) {
		return 0.f;
	}

}
