package assignment.activation;

public class Sigmoidal implements ActivationFunction {

	@Override
	public float activate(float x) {
		return (float) (1 / (1 + Math.pow(Math.E, -x)));
	}

	@Override
	public float derivative(float x) {
		float activation = this.activate(x);
		return activation * (1 - activation);
	}

}
