# Assignment 2

- Due date:  27th April, 2021 (Tuesday)
- Weightage: 15%
- Langauges: C/C++/Java only!

As part of this assignment, you are required to implement a Multilayer Perceptron (MLP).

Your code should be generic enough to handle:
1. Input of n-dimensional data
2. Any number of classes
3. Any number of hidden layers
4. Any activation function

You can assume a fully connected feed forward network.

Instructions to run :-
    1. Open up this project in Eclipse and hit the run button.

Implementation Details:-
    1. We have implemented MLP Layer using Layer Class
    2. Each Layer would be an instantiation of that class and all the adjacent classes are connected to one other just like a doubly linked list
    3. Layer class has a method called propagate which will calculate neuron output value and propagate it to next Layer
    4. It also has a method called backPropagate for propagating loss backwards and adjusting the weights with a given learning rate which is changeable
    5. MLP class connects all the layers and has methods for training and testing to which number of epochs,number of hidden layers,nodes per hidden layer,activation function of hidden layers and activation function of output layers would be supplied as arguments
    6. All the above arguments are user alterable and can be changed in Main.java
    7. Each neuron is implemented by Perceptron class
    8. Each neuron has a bias and weights of its connections from previous layers
    9. numClasses in Main.java is set to be 3 as we know iris dataset has 3 classes and harcoded the output labels for those 3 classes. If any other dataset is given we just need to change that variable and label hardcoding.